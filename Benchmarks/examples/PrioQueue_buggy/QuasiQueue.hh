/*
 * QuasiQueue.hh
 *  Created on: Jan 2, 2013
 *  Author: Arijit Chattopadhyay
 */

#ifndef QUASIQUEUE_HH_
#define QUASIQUEUE_HH_

#include<iostream>
#include<cstdlib>//For random function
#include<vector>//for the vector
#include<cmath> // for "abs" absolute value function

using namespace std;

#include "Lock.hh"
#include "Node.hh"

class NQuasiQueue {

private:
  int capacity;
  int head;
  int tail;
  Lock lock;
  vector<Node*> nodes;

public:
  NQuasiQueue(int _cap);
  void enqMethod(int item);
  int deqMethod();
  void showEntireQueue();
};

NQuasiQueue::NQuasiQueue(int _cap) {
  head = tail = 0;
  capacity = _cap;
  for (int i = 0; i < capacity; ++i) {
    Node *node = new Node();
    nodes.push_back(node);
  }
}


void NQuasiQueue::showEntireQueue() {
  for (int i = head; i < tail; ++i) {
    nodes[i]->show();
  }
}


void NQuasiQueue::enqMethod(int item) {
  if (capacity == abs(tail-head)) {
    cout<<"Can not enque in a full queue"<<endl;
    return;
  }
  int position = tail % capacity;
  nodes[position]->addElement(item);
  if(nodes[position]->getBucketSize() == nodes[position]->getMaxBucketSize()){
    ++tail;
  }
}

int NQuasiQueue::deqMethod() {
  lock.lock();
  if (head == tail) {
    Node *node =  nodes[tail];
    if(head == tail && node != NULL && node->getBucketSize() ==0){
      // cout<<"Can not deque from empty queue : "<<head<<":"<<tail<<endl;
      lock.unlock();
      return -999;
    }
  }

  Node *tempNode =  nodes[head%capacity];
  int retVal = tempNode->getElement();
  if(tempNode->getBucketSize() == 0){
    ++head;
  }
  lock.unlock();
  return retVal;
}

#endif /* QUASIQUEUE_HH_ */
