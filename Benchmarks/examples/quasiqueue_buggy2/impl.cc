/*
 * Author: Chao Wang <chaowang@vt.edu>
 *
 * Date: Jan 20, 2013
 *
 * Note: This implementation is buggy, because an enqueu may be over-taken
 * infinitely. 
 * 
 * 1.  What should have been implemented?  When a data item is deququed from a
 * segment, the flag of that data item should be set to FALSE.  When all data
 * items of that segment are marked FALSE, the segment itself is removed from
 * the linked list.
 *
 * 2.  What's been implemented?  What a data item is dequeued from a segment,
 * the vector of data items is shrinked by one.  If at this time, an enqueue
 * happens, it may actually be inserted into this segment.  
 *
 * This should not have been allowed, because one can actually alternate
 * (deq,enq)* forever, therefore over-taking the other data items in the same
 * segment by an infinite number of steps.
 * 
 */

#include<iostream>
#include<cstdlib>
using namespace std;
#include<pthread.h>
#include "QuasiQueue.hh"


NQuasiQueue pQueue(6);


void* thread1(void *)
{
  {
    pQueue.enqMethod(1);  
    cout<<"Enqueued : "<<1<<endl;
  }
  {
    pQueue.enqMethod(2);  
    cout<<"Enqueued : "<<2<<endl;
  }
  return 0;
}


void* thread2(void *)
{
  {
    pQueue.enqMethod(3);  
    cout<<"Enqueued : "<<3<<endl;
  }
  {
    int item = pQueue.deqMethod();  
    cout<<"                 Dequed : "<<item<<endl;
  }
  return 0;
}


int main(){

  pthread_t t1, t2;

  pQueue.enqMethod(0);  
  cout<<"Enqueued : "<<0<<endl<<endl;

  pthread_create( &t2, 0, thread2, 0);
  pthread_create( &t1, 0, thread1, 0);

  pthread_join( t1, 0 );
  pthread_join( t2, 0 );

  cout<<endl; 

  int item;
  item = pQueue.deqMethod();  
  cout<< "                 Dequed : "<<item<<endl;
  item = pQueue.deqMethod();  
  cout<<"                 Dequed : "<<item<<endl;
  item = pQueue.deqMethod();  
  cout<<"                 Dequed : "<<item<<endl;
  return 0;
}
