#ifndef LIN_PRIO_QUEUE
#define LIN_PRIO_QUEUE

#include "QuasiQueue.hh"


class PrioQueue{
private:
  int range;
  vector<NQuasiQueue*> prio_buckets;
  Lock lock;

public:
  PrioQueue(int r);
  void add(int item, int prio);
  int removeMin();
};

PrioQueue::PrioQueue(int r ){
  range = r;
  prio_buckets.get_allocator().allocate(range);
  for(int i = 0; i < range; ++i){
    NQuasiQueue *node = new NQuasiQueue(10);
    prio_buckets.push_back(node);
  }
}


void PrioQueue::add(int item, int prio){
  // lock.lock();
  prio_buckets[prio]->enqMethod(item);
  // lock.unlock();
}

int PrioQueue::removeMin(){
  int result = -999;
  lock.lock();
  for(int i = 0 ; i < range ; ++i){
    result = prio_buckets[i]->deqMethod();
    if(result != -999){
      lock.unlock();
      return result;
    }
  }
  lock.unlock();
  return result;
}

#endif
