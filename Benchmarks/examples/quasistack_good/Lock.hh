#ifndef LOCK_HH
#define LOCK_HH

#include<pthread.h>

class Lock {
private:
	pthread_mutex_t lk;
	pthread_cond_t condition;
public:
	Lock() {
		pthread_mutex_init(&lk, 0);
		pthread_cond_init(&condition, NULL);
	}
	~Lock() {
		pthread_mutex_destroy(&lk);
		pthread_cond_destroy(&condition);
	}
	void lock() {
		pthread_mutex_lock(&lk);
	}
	void unlock() {
		pthread_mutex_unlock(&lk);
	}
	void wait() {
		pthread_cond_wait(&condition, &lk);
	}
	void signal() {
		pthread_cond_signal(&condition);
	}
	void signalAll() {
		pthread_cond_broadcast(&condition);
	}
};

#endif
