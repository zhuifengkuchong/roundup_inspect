#include<pthread.h>
#include "QuasiStack.hh"

QuasiStack qStack(6);

void* thread1(void*){
  qStack.push(1);
  cout<<"Pushed : "<<1<<endl;  
  qStack.push(2);
  cout<<"Pushed : "<<2<<endl;
}

void* thread2(void*){
  qStack.push(3);
  cout<<"Pushed : "<<3<<endl;

  int item = qStack.pop();
  cout<<"               Popped : "<<item<<endl;
}


int main(){
  pthread_t t1, t2;
  qStack.push(0);
  cout<<"Pushed : "<<0<<endl;

  pthread_create(&t2,NULL,thread2,NULL);
  pthread_create(&t1,NULL,thread1,NULL);

  pthread_join(t1,NULL);
  pthread_join(t2,NULL);

  int item;

  item = qStack.pop();
  cout<<"               Popped : "<<item<<endl;
  item = qStack.pop();
  cout<<"               Popped : "<<item<<endl;
  item = qStack.pop();
  cout<<"               Popped : "<<item<<endl;
  return 0;
}
