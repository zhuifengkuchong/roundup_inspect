/*
 * QuasiStack.hh
 *
 *  Created on: Jan 3, 2013
 *      Author: arijit
 */

#ifndef QUASI_STACK_HH_
#define QUASI_STACK_HH_

#include "Node.hh"

class QuasiStack{
private: 
  Lock lock;
  int capacity;
  int top;
  vector<Node*> nodes;

public:
  QuasiStack(int cap);
  void push(int item);
  int pop();
};

QuasiStack::QuasiStack(int cap){
  capacity = cap;
  top = 0;
  for(int i = 0 ; i< cap ; ++i){
    Node *node = new Node();
    nodes.push_back(node);
  }
}

void QuasiStack::push(int item){
  if(top == (capacity)){
    cout<<"The Queue is full"<<endl;
  }else{
    lock.lock();
    int position = top % capacity;
    nodes[position]->addElement(item);
    if(nodes[position]->full()){
      ++top;
    }
    lock.unlock();
  }
}

int QuasiStack::pop(){
  int result =  -999;
  if(top == 0){
    cout<<"Queue is empty"<<endl;
  }else{
    lock.lock();
    Node *node = nodes[(top -1 )%capacity];
    result = node->getElement();
    if(node->empty()){
      --top;
    }
    lock.unlock();
  }
  return result;
}
#endif
