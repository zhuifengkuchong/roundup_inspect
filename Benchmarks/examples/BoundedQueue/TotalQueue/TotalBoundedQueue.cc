#include<iostream>
#include<vector>
#include<cstdlib>
using namespace std;

#include "TotalBoundedQueue.hh"
#include "../../Integer/Integer.hh"

TotalBoundedQueue<int> tQueue(10);
/*

 void* startOperation(void *f) {
 int *flag = (int*) f;
 cout<<"Flag"<<*flag<<endl;
 if (*flag % 2 == 0) {
 //do enq
 //		tQueue.enqMethod(rand() % 500);
 tQueue.enqMethod(*flag+500);
 } else {
 //do deq
 cout << "Deq value:"<<tQueue.deqMethod() << endl;
 }
 }
 */

void* doEnq(void *p) {
	int data = rand()%500;
	tQueue.enqMethod(data);
	cout<<"ENQED : "<<data<<endl;
}

void* doDeq(void *p){
	cout<<"Deq "<<tQueue.deqMethod()<<endl;
}

int main() {
	const int SIZE = 6;
	pthread_t threads[SIZE];

	for (int i = 0; i < SIZE; ++i) {
//		pthread_create(&threads[i], NULL, startOperation, (void*) &data);
		if(i%2 == 0){
			pthread_create(&threads[i], NULL,(void*) doEnq,NULL);
		}else{
			pthread_create(&threads[i], NULL,(void*)doDeq,NULL);
		}
	}
	for (int j = 0; j < SIZE; ++j) {
		pthread_join(threads[j], NULL);
	}

	tQueue.showEntireQueue();
	return 0;
}
