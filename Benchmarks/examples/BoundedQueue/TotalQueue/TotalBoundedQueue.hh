#ifndef TOTAL_BOUaNDEDQUEUE_H
#define TOTAL_BOUNDEDQUEUE_H

#include "../../AtomicInteger/AtomicInteger.hh"

class Lock {
private:
	pthread_mutex_t lk;
public:
	Lock() {
		pthread_mutex_init(&lk, 0);
	}
	~Lock() {
		pthread_mutex_destroy(&lk);
	}
	void lock() {
		pthread_mutex_lock(&lk);
	}
	void unlock() {
		pthread_mutex_unlock(&lk);
	}
};


template<class T>
class Node {
private:
	T value;
	Node<T> *next;
public:
	Node(T val);
	Node(T val, Node<T> *n);
	T getValue();
	void setValue(T val);
	Node<T>* getNext();
	void setNext(Node<T>* n);
};

template<class T>
Node<T>::Node(T val) {
	value = val;
	next = NULL;
}

template<class T>
Node<T>::Node(T val, Node<T> *n) {
	value = val;
	next = n;
}

template<class T>
T Node<T>::getValue() {
	return value;
}

template<class T>
void Node<T>::setValue(T val) {
	value = val;
}

template<class T>
Node<T>* Node<T>::getNext() {
	return next;
}

template<class T>
void Node<T>::setNext(Node<T> *n) {
	next = n;
}


template<class T>
class TotalBoundedQueue {
private:
	Node<T> *head;
	Node<T> *tail;
	int capacity;
	Lock enqLock, deqLock;
	AtomicInteger size;

public:
	TotalBoundedQueue(int cap);
	int getSize();
	int getCapacity();
	void enqMethod(T item);
	T deqMethod();
	void showEntireQueue();
};

template<class T>
void TotalBoundedQueue<T>::showEntireQueue(){
	cout<<"\n Showing the entire q: "<<endl;
	Node<T>* node = head->getNext();
	while(node != tail){
		cout<<node->getValue()<<endl;
		node = node->getNext();
	}
}

template<class T>
TotalBoundedQueue<T>::TotalBoundedQueue(int cap){
	capacity = cap;
	head = new Node<T>(-1);
	tail = new Node<T>(-1);
	tail = head;
}

template<class T>
int TotalBoundedQueue<T>::getSize(){
	return size.get();
}

template<class T>
int TotalBoundedQueue<T>::getCapacity(){
	return capacity;
}

template<class T>
void TotalBoundedQueue<T>::enqMethod(T item){
	enqLock.lock();
	try{
		if(size.get() == capacity){
			throw "Enq exception for full Queue";
		}
		Node<T> *node = new Node<T>(item);
		tail->setNext(node);
		tail = node;
		size.getAndIncrement();
		enqLock.unlock();
	}catch(const char *msg){
		enqLock.unlock();
		cout<<msg<<endl;
	}
}

template<class T>
T TotalBoundedQueue<T>::deqMethod() {
	T result;
	deqLock.lock();
	try {
		if(size.get() == 0) {
			throw "Deq from empty queue is not allowed\n";
		}
		size.getAndDecrement();
		result = head->getNext()->getValue();
//		head->setNext(head->getNext());
		head = head->getNext();
		deqLock.unlock();
	} catch(const char *msg) {
		deqLock.unlock();
		cout<<msg<<endl;
	}
	return result;
}

#endif
