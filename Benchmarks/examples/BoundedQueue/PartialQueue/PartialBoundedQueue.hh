#ifndef PARTIAL_BOUNDED_QUEUE_HH
#define PARTIAL_BOUNDED_QUEUE_HH

#include "../../AtomicInteger/AtomicInteger.hh"

class Lock {
private:
	pthread_mutex_t lk;
	pthread_cond_t condition;
public:
	Lock() {
		pthread_mutex_init(&lk, 0);
		pthread_cond_init(&condition, NULL);
	}
	~Lock() {
		pthread_mutex_destroy(&lk);
		pthread_cond_destroy(&condition);
	}
	void lock() {
		pthread_mutex_lock(&lk);
	}
	void unlock() {
		pthread_mutex_unlock(&lk);
	}
	void wait() {
		pthread_cond_wait(&condition, &lk);
	}
	void signal() {
		pthread_cond_signal(&condition);
	}
	void signalAll() {
		pthread_cond_broadcast(&condition);
	}
};

template<class T>
class Node {
private:
	T value;
	Node<T> *next;
public:
	Node(T val);
	Node(T val, Node<T> *n);
	T getValue();
	void setValue(T val);
	Node<T>* getNext();
	void setNext(Node<T>* n);
};

template<class T>
Node<T>::Node(T val) {
	value = val;
	next = NULL;
}

template<class T>
Node<T>::Node(T val, Node<T> *n) {
	value = val;
	next = n;
}

template<class T>
T Node<T>::getValue() {
	return value;
}

template<class T>
void Node<T>::setValue(T val) {
	value = val;
}

template<class T>
Node<T>* Node<T>::getNext() {
	return next;
}

template<class T>
void Node<T>::setNext(Node<T> *n) {
	next = n;
}

template<class T>
class PartialBoundedQueue {
private:
	int capacity;
	AtomicInteger size;
	Node<T> *head;
	Node<T> *tail;
	Lock enqLock;
	Lock deqLock;

public:
	PartialBoundedQueue(int cap);
	int getSize();
	int getCapacity();
	void enqMethod(T item);
	T deqMethod();
	void showEntireQueue();
};

template<class T>
void PartialBoundedQueue<T>::showEntireQueue(){
	Node<T> *node = head->getNext();
	while(node != NULL){
		cout<<node->getValue()<<endl;
		node = node->getNext();
	}
}

template<class T>
PartialBoundedQueue<T>::PartialBoundedQueue(int cap) {
	capacity = cap;
	head = new Node<T>(-1);
	tail = new Node<T>(-1);
	head = tail;
}

template<class T>
int PartialBoundedQueue<T>::getSize() {
	return size.get();
}

template<class T>
int PartialBoundedQueue<T>::getCapacity() {
	return capacity;
}

template<class T>
void PartialBoundedQueue<T>::enqMethod(T item) {
	enqLock.lock();
	bool wakeupDequer = false;
	try {
		while (size.get() == capacity) {
			cout << "Waiting for the full queue" << endl;
			enqLock.wait();
		}
		Node<T> *node = new Node<T>(item);
		tail->setNext(node);
		tail = node;
		if (size.getAndIncrement() == 0) {
			wakeupDequer = true;
		}
		enqLock.unlock();
		if (wakeupDequer) {
			deqLock.lock();
			try {
				deqLock.signalAll();
				deqLock.unlock();
			} catch (char *msgI) {
				cout << msgI << endl;
			}
		}
	} catch (char *msg) {
		cout << msg << endl;
	}
}
template<class T>
T PartialBoundedQueue<T>::deqMethod() {
	bool wakeupEnquer = false;
	T result;
	deqLock.lock();
	try {
		while (size.get() == 0) {
			cout << "Waiting for the enquer int empty queue" << endl;
			deqLock.wait();
		}
		result = head->getNext()->getValue();
		head = head->getNext();
		if (size.getAndDecrement() == capacity) {
					wakeupEnquer = true;
		}
		deqLock.unlock();

		if (wakeupEnquer) {
			enqLock.lock();
			try {
				enqLock.signalAll();
				enqLock.unlock();
			} catch (char *msgI) {
				cout << msgI << endl;
			}
		}
	} catch (char *msg) {
		cout << msg << endl;
	}
	return result;
}
#endif

