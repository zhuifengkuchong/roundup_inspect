#include<iostream>
#include<cstdlib>
using namespace std;

#include "PartialBoundedQueue.hh"

#include<pthread.h>

PartialBoundedQueue<int> pQueue(10);

void* doEnq(void *) {
	int data = rand() % 500;
	pQueue.enqMethod(data);
	cout << "Enqued :" << data << endl;
}

void *doDeq(void*) {
        int data =  pQueue.deqMethod();
	cout << "Dequed: " << data << endl;
}

int main(int argc, char **argv) {
//Sequential testing
/*
	for(int i = 0; i < 10;++i){
		pQueue.enqMethod(i);
	}

	for(int i = 0; i < 4;++i){
			pQueue.deqMethod();
	}

	pQueue.showEntireQueue();
*/

	const int SIZE = 20;
	pthread_t threads[SIZE];

	for (int i = 0; i < SIZE; ++i) {
		if (i % 2 == 0) {
			pthread_create(&threads[i], NULL, doEnq, NULL);
		} else {
			pthread_create(&threads[i], NULL, doDeq, NULL);
		}
	}
	for (int j = 0; j < SIZE; ++j) {
		pthread_join(threads[j], NULL);
	}

	return 0;
}
