#include<iostream>
#include<cstdlib>

using namespace std;

#include "QuasiStack.hh"
#include<pthread.h>

//#define SIZE  5
//---------------------------------------------------------------------
//chao: try to define "stack" as a global var, or 
//                       not  as a local var   
//  
//     INSPECT right now cannot handle "local class var" 
//---------------------------------------------------------------------

QuasiStack stack(6);  

void* doPush(void *){
  stack.push(3);
  cout<<"Pushed : "<<3<<endl;;
  stack.push(4);
  cout<<"Pushed : "<<4<<endl;
}

void* doPop(void *){
  stack.push(5);
  cout<<"Pushed : "<<5<<endl;

  int item = stack.pop();
  cout<<"                   Popped : "<<item<<endl;
}

int main(){
  /*
  const int SIZE =20;
  QuasiStack stack(SIZE);
  for(int  i = 0 ; i< SIZE ; ++i){
    stack.push(i);
  }

  stack.showEntireStack();
  for(int i = 0;  i< 5; ++i){
    stack.pop();
  }
  cout<<"After Pop"<<endl;
  stack.showEntireStack();

  */
  stack.push(1);
  cout<<"Pushed "<<1<<endl;
  stack.push(2);
  cout<<"Pushed "<<2<<endl;

  const int SIZE = 2;
  pthread_t threads[SIZE];
  for(int i = 0; i < SIZE ; i++){
    if(i %2 == 0 ){
      pthread_create(&threads[i], NULL, doPop, NULL);
    }else{
      pthread_create(&threads[i],NULL, doPush, NULL);
    }
  }
  
  for(int j = 0 ; j  < SIZE ; ++j){
    pthread_join(threads[j],NULL);
  }

  int item;
  item = stack.pop();
  cout<<"                   Popped : "<<item<<endl;
  item = stack.pop();
  cout<<"                   Popped : "<<item<<endl;
  item = stack.pop();
  cout<<"                   Popped : "<<item<<endl;
  item = stack.pop();
  cout<<"                   Popped : "<<item<<endl;
  
  return 0;
}
