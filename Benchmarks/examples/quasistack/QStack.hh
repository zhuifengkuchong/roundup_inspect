#ifndef QSTACK_HH_
#define QSTACK_HH_

#include "../AtomicInteger/AtomicInteger.hh"
#include "Lock.hh"


class QStack{

private:
  int capacity;
  //  AtomicInteger top;
  int top;
  vector<Node> nodes;
  Lock lock;

public:
  QStack(int cap);
  void push(int item);
  int pop();
  void showEntireStack();

};


QStack::QStack(int cap){
  capacity = cap;
  top = 0;

  for(int i = 0 ; i < capacity; ++i){
    Node node;
    nodes.push_back(node);
  }
}

void QStack::push(int item){
  if(capacity == top.get()){
    cout<<"Stack is full"<<endl;
    return;
  }
  
  nodes[top].addElement(item);
  top++;
}

int QStack::pop(){
  if(top == 0){
    cout<<"Stack is empty"<<endl;
    return;
  }

  lock.lock();
  
  lock.unlock();
}

#endif
