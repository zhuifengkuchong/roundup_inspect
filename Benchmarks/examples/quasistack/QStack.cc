#include<iostream>
#include<vector>
using namespace std;
#include<pthread.h>
#include "Lock.hh"

class QuasiStack{
private:
  vector<int> nodes;
  int top;
  int capacity;
  Lock lock;

public:
  QuasiStack(int cap);
  void push(int item);
  int pop();
  
  //Debugging function
  void showEntireStack();
};

QuasiStack::QuasiStack(int cap){
  capacity = cap;
  top = 0 ; 
}

void QuasiStack::push(int item){
  if(top ==  capacity){
    cout<<"Stack is full" ;
    return;
  }
  lock.lock();
  nodes.push_back(item);
  ++top;
  lock.unlock();
}

int QuasiStack::pop(){
  if(top == 0){
    cout<<"Pop is not allowed to emptyStacl"<<endl;
    return -999;
  }

  lock.lock();
  int item =  nodes.back();
  nodes.pop_back();
  --top;
  lock.unlock();
  return item;
}

QuasiStack stack(6);  

void* doPush(void *){
  stack.push(3);
  cout<<"Pushed : "<<3<<endl;;
  stack.push(4);
  cout<<"Pushed : "<<4<<endl;
}

void* doPop(void *){
  stack.push(5);
  cout<<"Pushed : "<<5<<endl;

  int item = stack.pop();
  cout<<"                   Popped : "<<item<<endl;
}

int main(){
  stack.push(1);
  cout<<"Pushed "<<1<<endl;
  stack.push(2);
  cout<<"Pushed "<<2<<endl;

  const int SIZE = 2;
  pthread_t threads[SIZE];
  for(int i = 0; i < SIZE ; i++){
    if(i %2 == 0 ){
      pthread_create(&threads[i], NULL, doPush, NULL);
    }else{
      pthread_create(&threads[i],NULL, doPop, NULL);
    }
  }
  
  for(int j = 0 ; j  < SIZE ; ++j){
    pthread_join(threads[j],NULL);
  }
  int item;
  item = stack.pop();
  cout<<"                   Popped : "<<item<<endl;
  item = stack.pop();
  cout<<"                   Popped : "<<item<<endl;
  item = stack.pop();
  cout<<"                   Popped : "<<item<<endl;
  item = stack.pop();
  cout<<"                   Popped : "<<item<<endl;

  return 0;
}
