/*
 * Node.hh
 *
 *  Created on: Jan 3, 2013
 *      Author: arijit
 */

#ifndef NODE_HH_
#define NODE_HH_

#include<iostream>
#include<vector>
using namespace std;

#include "Lock.hh"

const int MAX_BUCKET_SIZE = 2;

class Node {

private:
  vector<int> bucket;
  Lock lock;
  int SIZE;

public:
  Node();
  Node(const Node &n);
  void addElement(int elem);
  int getElement();
  int getBucketSize();
  int getMaxBucketSize();

  // Development functions
  void show();
  void removeElement();
  void removeElement(int index);
};

//Default Constructor

Node::Node():SIZE(MAX_BUCKET_SIZE) {}

//Copy Constructor
Node::Node(const Node &n):SIZE(MAX_BUCKET_SIZE){
  cout<<"Copy Constructor"<<endl;
  bucket.clear();
  for(int i = 0 ; i < n.bucket.size(); ++i){
    bucket.push_back(n.bucket[i]);
  }
  lock  = n.lock;
}


//Debug Functions start
void Node::show() {
  for (int i = 0; i < bucket.size(); ++i) {
    cout << bucket[i] << "\t";
  }
  cout<<endl;
}

void Node::removeElement(){
  bucket.erase(bucket.begin());
}

void Node::removeElement(int index){
  bucket.erase(bucket.begin()+(index));
}

//Debug function ends


void Node::addElement(int elem) {
  if(bucket.size() < SIZE){
    lock.lock();
    bucket.push_back(elem);
    lock.unlock();
  }
}

int Node::getElement() {
  int result = -999;
  lock.lock();
  if(bucket.size() == 0 ){
    lock.unlock();
    return result;
  }
  int position = rand() % bucket.size();
  //cout<<"Position : "<<position<<endl;
  result = bucket[position];
  if(position == 0){
    bucket.erase(bucket.begin());
  }else{
    bucket.erase(bucket.begin()+(position-1));
  }
  lock.unlock();
  return result;
}

int Node::getBucketSize(){
  return bucket.size();
}

int Node::getMaxBucketSize(){
  return MAX_BUCKET_SIZE;
}

#endif /* NODE_HH_ */
