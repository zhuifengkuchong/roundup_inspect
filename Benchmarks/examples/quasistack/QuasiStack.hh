#ifndef QUASI_STACK_HH_
#define QUASI_STACK_HH_

#include <iostream>
#include<vector>
using namespace std;

#include "Node.hh"
#include "Lock.hh"

class QuasiStack{

private:
  vector<Node*> nodes;
  int top;
  int capacity;
  Lock lock;

public:
  QuasiStack(int cap);
  void push(int item);
  int pop();
  
  //Debugging function
  void showEntireStack();
};

QuasiStack::QuasiStack(int cap){
  capacity = cap;
  top = 0;
  for(int i = 0 ; i < capacity; ++i){
    Node* node = new Node();
    nodes.push_back(node);
  }
}

void QuasiStack::push(int item){
  if(top == capacity){
    //cout<<"Can not push into a full stack"<<endl;
    return;
  }
  lock.lock();
  Node *topNode =  nodes[top];
  topNode->addElement(item);
  if(topNode->getBucketSize() == topNode->getMaxBucketSize()){
    top++;
  }
  lock.unlock();
}

int QuasiStack::pop(){
  lock.lock();
  if(top == 0 ){
    Node *node = nodes[top];
    if(top == 0 && node != NULL && node->getBucketSize() == 0){
      //cout<<"Can not pop from the empty stack"<<top<<endl;
      lock.unlock();
      return -999;
    }
  }
  //cout<<"Top is : "<<top<<endl;
  Node *node  = nodes[top-1];
  int result = node->getElement();
  if(node->getBucketSize() == 0 ){
    --top;
  }
  lock.unlock();
  return result;
}


void QuasiStack::showEntireStack(){
  vector<Node*>::iterator itr = nodes.begin();
  while(itr != nodes.end()){
    (*itr)->show();
    ++itr;
  }
}

#endif
