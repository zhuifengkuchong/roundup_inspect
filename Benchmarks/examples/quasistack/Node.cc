#include<iostream>
#include<cstdlib>

using namespace std;
#include "Node.hh"

int main(){
  Node n;
  for(int i = 0 ; i< 10; ++i){
    n.addElement(i);
  }
  n.show();
  for(int i = 0 ; i < 5; i++){
    n.getElement();
  }
  Node t = n;
  // t.show();
  t.removeElement();
  t.show();
  // n.show();
  cout<<"Removing after index 1"<<endl;
  t.removeElement(1);
  t.show();
  return 0;
}
