make clean;  make
echo 
echo LOC:
find . -name '*.cc' -o -name '*.hh' | xargs wc -l

echo 
echo linCheck 0:
time ../../../inspect-0.3/inspect --linCheck 0 ./spec.exe  >& result0.txt 

echo 
echo linCheck 1 time:

time ../../../inspect-0.3/inspect --linCheck 1 ./impl.exe  >& result1.txt 


echo 
echo linearizations in linCheck 1:

cd to_check_trace/

ls -lR|grep "txt"|wc -l

cd ..



echo 
echo linCheck 2 1 time:

time ../../../inspect-0.3/inspect --linCheck 2 1 ./impl.exe  >& result2_1.txt 

echo 
echo mutations in linCheck 2 1:

cd to_check_quasi_trace/

ls -lR|grep "txt"|wc -l

cd ..


echo 
echo linCheck 2 2 time:

time ../../../inspect-0.3/inspect --linCheck 2 2 ./impl.exe  >& result2_2.txt 

echo 
echo mutations in linCheck 2 2:

cd to_check_quasi_trace/

ls -lR|grep "txt"|wc -l

cd ..


echo 
echo linCheck 2 3 time:

time ../../../inspect-0.3/inspect --linCheck 2 3 ./impl.exe  >& result2_3.txt 

echo 
echo mutations in linCheck 2 3:

cd to_check_quasi_trace/


ls -lR|grep "txt"|wc -l

cd ..

echo
make clean

