//-----------------------------------------------------------------------------
// Author:  Chao Wang <chaowang@vt.edu>
// Date:  12/27/2012
// 
// An example Linux/C++ implementation of the 'AtomicReference' class used in
// the book: The Art of Multiprocessor Programming.
//-----------------------------------------------------------------------------

#ifndef  MP_ATOMIC_REFERENCE_HH
#define  MP_ATOMIC_REFERENCE_HH

#include <cstddef>

template <class T> class AtomicReference {
  T* ref;

public:
  AtomicReference() { ref = NULL; }
  AtomicReference(T* ptr) { ref = ptr; }
  ~AtomicReference() { }

  T* get() { 
    return ref;
  }

  void set(T* update) { 
    ref = update; 
  }

  T* getAndSet(T* update) {
    // call GCC built-in function (corresponding to 'atomicrmw' in LLVM)
    return __sync_lock_test_and_set(&ref, update);
  }

};

#endif // MP_ATOMIC_REFERENCE_HH
