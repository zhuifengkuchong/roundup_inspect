//
// This is a test program for 'AtomicReference' class defined in the .hh file
//
// Due to the use of 'AtomicReference::compareAndSet()' and the do-while loop,
// the final value of x is either 10, or 11, but not 1.
//

#include <pthread.h>
#include <stdio.h>
#include <assert.h>

#include "../Integer/Integer.hh"
#include "../AtomicReference/AtomicReference.hh"

Integer i1(1);;
Integer i10(10);

AtomicReference<Integer> x;

int main(void)
{
  printf("\n main ---------\n");
  x.set( &i1 );
  Integer *p = x.getAndSet( &i10 );
  Integer *q = x.get();
  printf("\n main ---------\n");

  assert( p->get() == 1 && q->get() == 10 && "p == &i1,  q == &i10" );

  return 0;
}

 
