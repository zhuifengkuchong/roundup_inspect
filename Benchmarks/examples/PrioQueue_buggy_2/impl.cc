#include <pthread.h>
#include  "LinearPrioQueue.hh"


PrioQueue prioQ(6);

void* insert(void*){
  prioQ.add(2,1);
  cout<<"Added Element : "<<2<<" With Priority : "<< 1<<endl;
  prioQ.add(3,1);
  cout<<"Added Element : "<<3<<" With Priority : "<< 1<<endl;  
}

void* remove(void *){
  prioQ.add(4,1);
  cout<<"Added Element : "<<4<<" With Priority : "<< 1<<endl;

  int item = prioQ.removeMin();
  cout<<"                                          Removed : "<<item<<endl;
}

int main(){

  pthread_t t1,t2;
  //Argument sequence item, prio
  prioQ.add(1,1);
  cout<<"Added Element : "<<1<<" With Priority : "<< 1<<endl;

  pthread_create(&t2,NULL,remove,NULL);
  pthread_create(&t1,NULL,insert,NULL);

  pthread_join(t1,NULL);
  pthread_join(t2,NULL);
 
  int item;
  item = prioQ.removeMin();
  cout<<"                                          Removed : "<<item<<endl;
  item = prioQ.removeMin();
  cout<<"                                          Removed : "<<item<<endl;
  item = prioQ.removeMin();
  cout<<"                                          Removed : "<<item<<endl;

  return 0;
}

