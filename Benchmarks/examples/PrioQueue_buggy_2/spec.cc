#include<iostream>
#include<deque>
#include<vector>
#include<cstdlib>
#include<cmath>
using namespace std;

#include<pthread.h>
#include "Lock.hh"
#define DEBUG false

class Queue{
private:
  int capacity;
  int head;
  int tail;
  Lock lock;
  deque<int> nodes;

public:
  Queue(int cap);
  void enqMethod(int item);
  int deqMethod();

  //debug function
  void show(){
    if(!DEBUG) return;
    cout<<"\n Show \n";
    for(int i = 0 ; i < nodes.size(); ++i){
      cout<<nodes[i]<<"\t";
    }
    cout<<endl;
  }
};

Queue::Queue(int cap){
  tail = 0;
  head = 0;
  capacity = cap;
  nodes.get_allocator().allocate(capacity);
}

void Queue::enqMethod(int item){
  if(capacity == abs(tail -head)){
    //    cout<<"Queue is full"<<endl;
  }else{
    lock.lock();
    nodes.push_back(item);
    tail++;
    show();
    lock.unlock();
  }
}

int Queue::deqMethod(){
  int result = -999;
  lock.lock();
  if(tail == head){
    //   cout<<"Queue is empty"<<endl;
    show();
    lock.unlock();
  }else{
    result = nodes.front();
    nodes.pop_front();
    head++;
    show();
    lock.unlock();
  }
  return result;
}


class PrioQueue{
private:
  int range;
  vector<Queue*> prio_buckets;
  Lock lock;

public:
  PrioQueue(int r);
  void add(int item, int prio);
  int removeMin();
};

PrioQueue::PrioQueue(int r){
  range = r;
  prio_buckets.get_allocator().allocate(range);
  for(int i = 0 ; i < range; ++i){
    Queue *queue = new Queue(10);
    prio_buckets.push_back(queue);
  }
}

void PrioQueue::add(int item, int prio){
  lock.lock();
  prio_buckets[prio]->enqMethod(item);
  lock.unlock();
}

int PrioQueue::removeMin(){
  int result = -999;
  lock.lock();
  for(int i = 0 ; i < range ; ++i){
    result = prio_buckets[i]->deqMethod();
    if(result != -999){
      lock.unlock();
      return result;
    }
  }
  lock.unlock();
  return result;
}

PrioQueue prioQ(6);

void* insert(void*){
  prioQ.add(2,1);
  cout<<"Added Element : "<<2<<" With Priority : "<< 1<<endl;
  prioQ.add(3,1);
  cout<<"Added Element : "<<3<<" With Priority : "<< 1<<endl;  
}

void* remove(void *){
  prioQ.add(4,1);
  cout<<"Added Element : "<<4<<" With Priority : "<< 1<<endl;

  int item = prioQ.removeMin();
  cout<<"                                          Removed : "<<item<<endl;
}

int main(){

  pthread_t t1,t2;
  //Argument sequence item, prio
  prioQ.add(1,1);
  cout<<"Added Element : "<<1<<" With Priority : "<< 1<<endl;

  pthread_create(&t2,NULL,remove,NULL);
  pthread_create(&t1,NULL,insert,NULL);

  pthread_join(t1,NULL);
  pthread_join(t2,NULL);
 
  int item;
  item = prioQ.removeMin();
  cout<<"                                          Removed : "<<item<<endl;
  item = prioQ.removeMin();
  cout<<"                                          Removed : "<<item<<endl;
  item = prioQ.removeMin();
  cout<<"                                          Removed : "<<item<<endl;

  return 0;
}

