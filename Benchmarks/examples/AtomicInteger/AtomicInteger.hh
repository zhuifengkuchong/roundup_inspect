//-----------------------------------------------------------------------------
// Author:  Chao Wang <chaowang@vt.edu>
// Date:  12/27/2012
// 
// An example Linux/C++ implementation of the 'AtomicInteger' class used in
// the book: The Art of Multiprocessor Programming.
//-----------------------------------------------------------------------------

#ifndef  MP_ATOMIC_INTEGER_HH
#define  MP_ATOMIC_INTEGER_HH

class AtomicInteger {
  int value;

public:
  AtomicInteger() { 
    value = 0; 
  }
  ~AtomicInteger() { }

  int get() { 
    return value; 
  }

  void set(int update) { 
    value = update; 
  }

  bool compareAndSet(int expect, int update) {
    // call GCC built-in function (corresponding to 'cmpxchg' in LLVM)
    bool res =  __sync_bool_compare_and_swap(&value, expect, update);
    return res;
  }

  int getAndIncrement() {
    // call GCC built-in function (corresponding to 'cmpxchg' in LLVM)
    return __sync_fetch_and_add(&value, 1);
  }

  int getAndDecrement(){
	  // call GCC built-in function (corresponding to 'cmpxchg' in LLVM)
	      return __sync_fetch_and_sub(&value,1);
  }

};

#endif // MP_ATOMIC_INTEGER_HH
