//
// This is a test program for 'AtomicInteger' class defined in the .hh file
//
// Due to the use of 'AtomicInteger::compareAndSet()' and the do-while loop,
// the final value of x is either 10, or 11, but not 1.
//

#include <pthread.h>
#include <stdio.h>
#include <assert.h>

#include "AtomicInteger.hh"

AtomicInteger x;

void* foo (void*)
{
  int a;
  bool flag;

  do {
    a = x.get();
  }while ( ! x.compareAndSet(a,a+1) );

  return 0;
}

void* bar (void*)
{
  x.set(10);
  return 0;
}

int main(void)
{
  pthread_t t1, t2;

  x.set(0);

  pthread_create(&t1, 0, foo, 0);
  pthread_create(&t2, 0, bar, 0);
  pthread_join(t1,0);
  pthread_join(t2,0);

  int val = x.get();
  
  assert( (val==10 || val==11) && "x should be either 10 or 11 (but not 1)" );

  return 0;
}

 
