#include<iostream>
#include<vector>
#include<cmath>
#include<cstdlib>
using namespace std;
#include<pthread.h>
#include "Lock.hh"

class NQuasiQueue {
private:
  int capacity;
  int head;
  int tail;
  Lock lock;
  vector<int> nodes;

public:
  NQuasiQueue(int _cap);
  void enqMethod(int item);
  int deqMethod();
  void showEntireQueue();
};

NQuasiQueue::NQuasiQueue(int _cap) {
  head = tail = 0;
  capacity = _cap;
  nodes.get_allocator().allocate(capacity);
}


void NQuasiQueue::showEntireQueue() {
  for (int i = head; i < tail; ++i) {
    cout<<nodes[i]<<"\t";
  }
  cout<<endl;
}


void NQuasiQueue::enqMethod(int item) {
  lock.lock();
  if (capacity == abs(tail-head)) {
    cout<<"Can not enque in a full queue"<<endl;
    lock.unlock();
    return;
  }
  int position = tail % capacity;
  nodes.push_back(item);
  ++tail;
  lock.unlock();
}

int NQuasiQueue::deqMethod() {
  lock.lock();
  if (head == tail) {
      cout<<"Can not deque from empty queue"<<endl;
      lock.unlock();
      return -999;
  }

  int tempNode =  nodes[head%capacity];
   ++head;
  lock.unlock();
  return tempNode;
}



// NQuasiQueue pQueue(6);

// void* doEnq(void *){
//   pQueue.enqMethod(3);
//   cout<<"Enqueued : "<<3<<endl;
//   pQueue.enqMethod(4);
//   cout<<"Enqueued : "<<4<<endl;
// }

// void* doDeq(void *){
//   pQueue.enqMethod(5);
//   cout<<"Enqueued : "<<5<<endl;
  
//   int item = pQueue.deqMethod();
//   cout<<"Dequed : "<<item<<endl;
// }

// int main(){
//   //Parallel Testing
//   const int SIZE =  2; //chao: use only TWO threads for now
//   pthread_t threads[SIZE];

//   for(int i = 0 ; i <SIZE; ++i){
//     if(i % 2 == 0){
//       pthread_create(&threads[i],NULL,doEnq,NULL);
//     }else{
//       pthread_create(&threads[i],NULL,doDeq,NULL);
//     }
//   }

//   for(int j = 0 ; j < SIZE; ++j){
//       pthread_join(threads[j],NULL);
//   }
//   // cout<<"\n\n ************End************\n\n";
//   return 0;
// }

// NQuasiQueue pQueue(6);

// void* doEnq(void *){
//   pQueue.enqMethod(3);
//   cout<<"Enqueued : "<<3<<endl;
//   int item =  pQueue.deqMethod();
//   cout<<"Dequeued : "<<item<<endl;
// }

// void* doEnq1(void*){
//   pQueue.enqMethod(4);
//   cout<<"Enqueued : "<<4<<endl;
//   int item =  pQueue.deqMethod();
//   cout<<"Dequeued : "<<item<<endl;
// }

// void* doDeq(void *){
//   pQueue.enqMethod(5);
//   cout<<"Enqueued : "<<5<<endl;
  
//   // int item = pQueue.deqMethod();
//   // cout<<"Dequed : "<<item<<endl;
// }

// int main(){
   
//   //Parallel Testing
//   const int SIZE =  2; //chao: use only TWO threads for now
//   pthread_t threads[SIZE+1];
//   //  pQueue.enqMethod(1);
//   pQueue.enqMethod(2);

//   for(int i = 0 ; i <SIZE; ++i){
//     if(i % 2 == 0){
//       pthread_create(&threads[i],NULL,doEnq,NULL);
//       pthread_create(&threads[i+1],NULL,doEnq1,NULL);
//     }else{
//       pthread_create(&threads[i+1],NULL,doDeq,NULL);
//     }
//   }

//   for(int j = 0 ; j < SIZE+1; ++j){
//       pthread_join(threads[j],NULL);
//   }
//   // cout<<"\n\n ************End************\n\n";
//   return 0;
// }

NQuasiQueue pQueue(6);

void* doEnq(void *){
  pQueue.enqMethod(3);
  cout<<"Enqueued : "<<3<<endl;
  pQueue.enqMethod(4);
  cout<<"Enqueued : "<<4<<endl;
}


void* doDeq(void *){
  pQueue.enqMethod(5);
  cout<<"Enqueued : "<<5<<endl;
  int item = pQueue.deqMethod();
  cout<<"Dequed : "<<item<<endl;
}

int main(){
   
  //Parallel Testing
  const int SIZE =  2; //chao: use only TWO threads for now
  pthread_t threads[SIZE];
  // pQueue.enqMethod(1);
  // pQueue.enqMethod(2);

  for(int i = 0 ; i <SIZE; ++i){
    if(i % 2 == 0){
      pthread_create(&threads[i],NULL,doEnq,NULL);
    }else{
      pthread_create(&threads[i],NULL,doDeq,NULL);
    }
  }

  for(int j = 0 ; j < SIZE; ++j){
    pthread_join(threads[j],NULL);
  }
  return 0;
}
