1.  Test Program: the main() function

It need to have the same number of enqMethod() and deqMthod()

Thread 0
--------
enqMethod(0)
enqMethod(1)
enqMethod(2)

            Thread 1
            -------
            enqMethod(3)
            enqMethod(4)

                          Thread 2
                          --------
                          enqMethod(5)
                          deqMethod()

deqMethod()
deqMethod()
deqMethod()
deqMethod()
deqMethod()


2.  Removing the non-demininism in "Quasi" data structure


   int idx = rand()%elements.size();

Should be re-written into 

   int idx = elements.size() - 1;

This will create the maximum out-of-order movement, while at the same time
not introducing non-determinism.

If there are non-determinism in "test.exe", Inspect is bound to fail, because it
can no longer forece "test.exe" to execute a certain interleaving.


3.  Take a look at the new Makefile

-----------------------------------------------
C=../../../bin/insp++ --linCheck --savetmp

all: 
	$(CC) spec.cc -o spec.exe
	$(CC) impl.cc -o impl.exe
clean:
	rm -rf *.o
	rm *.lz.*
	rm $(TARGET)
-----------------------------------------------


4. How to run the test?


    make clean;  make

    ../../../inspect-0.3/inspect --linCheck 0 ./spec.exe  >& result0.txt &

    ../../../inspect-0.3/inspect --linCheck 1 ./impl.exe  >& result1.txt &

    ../../../inspect-0.3/inspect --linCheck 2 2 ./impl.exe  >& result2_2.txt &

    
