#include<iostream>
#include<cstdlib>
using namespace std;
#include<pthread.h>
#include "QuasiQueue.hh"

// NQuasiQueue pQueue(6);

// void* doEnq(void *){
//   pQueue.enqMethod(3);
//   cout<<"Enqueued : "<<3<<endl;
//   // pQueue.enqMethod(4);
//   // cout<<"Enqueued : "<<4<<endl;
// }

// void* doEnq1(void*){
//   pQueue.enqMethod(4);
//   cout<<"Enqueued : "<<4<<endl;
// }

// void* doDeq(void *){
//   pQueue.enqMethod(5);
//   cout<<"Enqueued : "<<5<<endl;
  
//   int item = pQueue.deqMethod();
//   cout<<"Dequed : "<<item<<endl;
// }

// int main(){
//   // cout<<"\n\n ************Start************\n\n";
//   /*
 
//   //Sequential Testing
//   NQuasiQueue queue(20);
//   for(int  i  =0 ; i < 10 ; ++i){
//   queue.enqMethod(i);
//   }
//   queue.showEntireQueue();
//   cout<<endl;
//   for(int  i  =0 ; i < 2 ; ++i){
//   queue.deqMethod();
//   }
//   cout<<"After Deque operation"<<endl;
//   queue.showEntireQueue();
//   cout<<endl;
//   */

//   //chao: ENQ two items first, to avoid DEQ an empty queue later
//   //pQueue.enqMethod(1);  
//   //pQueue.enqMethod(2);

    
//   //Parallel Testing
//   const int SIZE =  2; //chao: use only TWO threads for now
//   pthread_t threads[SIZE+1];
//   pQueue.enqMethod(1);
//   pQueue.enqMethod(2);

//   for(int i = 0 ; i <SIZE; ++i){
//     if(i % 2 == 0){
//       pthread_create(&threads[i],NULL,doEnq,NULL);
//       pthread_create(&threads[i+1],NULL,doEnq1,NULL);
//     }else{
//       pthread_create(&threads[i],NULL,doDeq,NULL);
//     }
//   }

//   for(int j = 0 ; j < SIZE+1; ++j){
//     pthread_join(threads[j],NULL);
//   }
//   // cout<<"\n\n ************End************\n\n";
//   return 0;
// }



NQuasiQueue pQueue(6);

void* doEnq(void *){
  pQueue.enqMethod(3);
  cout<<"Enqueued : "<<3<<endl;
  pQueue.enqMethod(4);
  cout<<"Enqueued : "<<4<<endl;
}

void* doDeq(void *){
  pQueue.enqMethod(5);
  cout<<"Enqueued : "<<5<<endl;
  
  int item = pQueue.deqMethod();
  cout<<"Dequed : "<<item<<endl;
}

int main(){
   
  //Parallel Testing
  const int SIZE =  2; //chao: use only TWO threads for now
  pthread_t threads[SIZE];
  // pQueue.enqMethod(1);
  // pQueue.enqMethod(2);

  for(int i = 0 ; i <SIZE; ++i){
    if(i % 2 == 0){
      pthread_create(&threads[i],NULL,doEnq,NULL);
    }else{
      pthread_create(&threads[i],NULL,doDeq,NULL);
    }
  }

  for(int j = 0 ; j < SIZE; ++j){
    pthread_join(threads[j],NULL);
  }
  return 0;
}
