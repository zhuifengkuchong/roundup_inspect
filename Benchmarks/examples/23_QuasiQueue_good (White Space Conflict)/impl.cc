/*
 * Author: Chao Wang <chaowang@vt.edu>
 *
 * Date: Jan 20, 2013
 *
 * Note: This implementation is correct, but not efficient.
 *
 * 
 * 1.  The enqMethod() and deqMethod() are protected by the same lock.
 *
 * 2.  However, if we remove the lock in the enqMethod(), there will be data
 *     races on "tail" and "head" fields -- leading to a wrong implementation.
 *     
 */

#include<iostream>
#include<cstdlib>
using namespace std;
#include<pthread.h>
#include "QuasiQueue.hh"


NQuasiQueue pQueue(6);




void* thread1(void *)
{
  {
    pQueue.enqMethod(1);  
    cout<<"Enqueued : "<< 1 <<endl;
  }
  {
    pQueue.enqMethod(2);  
    cout<<"Enqueued : "<< 2 <<endl;
  }
  {
    int item = pQueue.deqMethod();  
    cout<<"                 Dequed : "<<item<<endl;
  }
  return 0;
}


void* thread2(void *)
{
  {
    pQueue.enqMethod(3);  
    cout<<"Enqueued : "<< 3 <<endl;
  }
  {
    int item = pQueue.deqMethod();  
    cout<<"                 Dequed : "<<item<<endl;
  }
  return 0;
}


int main(){

  pthread_t t1, t2;

  pQueue.enqMethod(0);  
  cout<<"Enqueued : "<< 0 <<endl<<endl;

  pthread_create( &t2, 0, thread2, 0);
  pthread_create( &t1, 0, thread1, 0);

  pthread_join( t1, 0 );
  pthread_join( t2, 0 );

  cout<<endl; 

  int item;
  item = pQueue.deqMethod();  
  cout<< "                 Dequed : "<<item<<endl;
  item = pQueue.deqMethod();  
  cout<<"                 Dequed : "<<item<<endl;

  return 0;
}
