//
// This is a test program for 'CoarseList' class defined in the .hh file
//

#include "../CoarseList/CoarseList.hh"
#include <pthread.h>
#include <stdio.h>
#include <assert.h>
#include <iostream>
using namespace std;

//----- This will be the coarse-grain list -----
CoarseList<int>  list;

void* foo (void*)
{
  bool res = list.add( 10 );
  cout << "list.add: " << 10 << endl;  
  assert( res );

  return 0;
}

void* bar (void*)
{
  bool res1 = list.add( 20 );
  cout << "list.add: " << 20 << endl;  
  assert( res1 );

  bool res2 = list.remove ( 20 );
  cout << "list.remove: " << 20 << endl;  
  assert( res2 );

  return 0;
}

int main(void)
{

  pthread_t t1, t2;
  pthread_create(&t1, 0, foo, 0);
  pthread_create(&t2, 0, bar, 0);

  pthread_join(t1,0);
  pthread_join(t2,0);
  

  bool res3 = list.contains ( 10 );
  cout << "list.contains(" << 10 << ") = " << res3 << endl;  
  bool res4 = list.contains ( 20 );
  cout << "list.contains(" << 20 << ") = " << res4 << endl;  

  assert( res3 && !res4 && "list should contain 10 (but not 20)" );

  return 0;
}

 
