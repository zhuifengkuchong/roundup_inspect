//-----------------------------------------------------------------------------
// Author:  Chao Wang <chaowang@vt.edu>
// Date:  12/27/2012
// 
// An example Linux/C++ implementation of the 'ListSet' class used in Chapter 9
// of the book: The Art of Multiprocessor Programming (Herlihy & Shavit).
//
// NOTE:  C++ does not support "try {...} finally     {...}"
//
//        We simulate it with  "try {...} catch(...){}{ ...}"
// -----------------------------------------------------------------------------

#ifndef  MP_LIST_SET_HH
#define  MP_LIST_SET_HH

#include <stdlib.h>
#include <cstddef>
#include <pthread.h>
#include <stdio.h>

#define INT_MIN_VALUE   -1024
#define INT_MAX_VALUE   +1024


template <class T> class Node {
  T item;
public:
  int key;
  Node<T> *next;
public:
  Node()       { item = NULL; key = 0 ; next = NULL; }
  Node(int ke) { item = NULL; key = ke; next = NULL; }
  //  Node(T it )  { item = it  ; key = it->hashCode();  next = NULL; }
};


class Lock {
  pthread_mutex_t  lk;
public:
  Lock()        { pthread_mutex_init   (&lk, 0); }
  ~Lock()       { pthread_mutex_destroy(&lk); }
  void lock()   { pthread_mutex_lock   (&lk); }
  void unlock() { pthread_mutex_unlock (&lk); }
};


template <class T> class CoarseList {
  Node<T> *head;
  Lock lock;

public:

  CoarseList() {
    head = new Node<T>(INT_MIN_VALUE);
    head->next = new Node<T>(INT_MAX_VALUE);
  }

  bool add(T item) {
    Node<T> *pred, *curr;
    int key = item;//item->hashCode();
    lock.lock();
    //try {
    pred = head;
    curr = pred->next;
    while (curr->key < key) {
      pred = curr;
      curr = curr->next;
    }
    if (key == curr->key) {
      lock.unlock();
      return false;
    } else {
      Node<T> *node = new Node<T>(item);
      node->next = curr;
      pred->next = node;
      lock.unlock();
      return true;
    }
    //} finally { 
    //  lock.unlock();
    //}
  }

  bool remove(T item) {
    Node<T> *pred, *curr;
    int key = item;//item->hashCode();
    lock.lock();
    //try {
    pred = head;
    curr = pred->next;
    while (curr->key < key) {
      pred = curr;
      curr = curr->next;
    }
    if (key == curr->key) {
      pred->next = curr->next;
      lock.unlock();
      return true;
    } else {
      lock.unlock();
      return false;
    }
    //} finally {  
    //lock.unlock();
    //}
  }

  bool contains(T item) {
    Node<T> *pred, *curr;
    int key = item;//item->hashCode();
    lock.lock();
    //try {
    pred = head;
    curr = pred->next;
    while (curr->key < key) {
      pred = curr;
      curr = curr->next;
    }
    if (key == curr->key) {
      lock.unlock();
      return true;
    } else {
      lock.unlock();
      return false;
    }
    //} finally {  
    //  lock.unlock();
    //}
  }

};

#endif // MP_LIST_SET_HH
