#ifndef BIN_HH_
#define BIN_HH_

#include<vector>
#include<ctime>
using namespace std;

#include "Lock.hh"

const int MAX_SIZE 2;

class Bin{
private:
  Lock lock;
  vector<int> itemList;

public :
  Bin();
  void put(int item);
  int get();
  int getBucketSize();
  int getMaxBucketSize();
};


Bin::Bin(){
  srand(time(0));
  itemList.get_allocator().allocate(MAX_SIZE);
}

void Bin::put(int item){
  lock.lock();
  itemList.push_back(item);
  lock.unlock();
}

int Bin::get(){
  lock.lock();
  int range = itemList.size();
  if(range == 0){
    lock.unlock();
    return -999;
  }

  int location = rand()%range;
  int result = itemList[location];
  if(location == 0 ){
    itemList.erase(itemList.begin());
  }else{
    itemList.erase(itemList.begin()+(location-1));
  }
  return result;
}

int Bin::getBucketSize(){
  return itemList.size();
}

int Bin::getMaxBucketSize(){
  return MAX_SIZE;
}


#endif
