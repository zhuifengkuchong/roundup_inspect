#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

#include<pthread.h>
#include "LinearPrioQueue.hh"

const int MAX =  4;
PrioQueue pQueue(MAX);

void* insert(void*){
  pQueue.add(3,1);
  cout<<"Inserted 3 with prio 1"<<endl;
  pQueue.add(4,1);
  cout<<"Inserted 4 with prio 1"<<endl;
}

void* remove(void*){
  pQueue.add(5,1);
  cout<<"Inserted 5 with prio 1"<<endl;
  int item = pQueue.removeMin();
  cout<<"Item that is removed : "<<item<<endl;
}

int main(){
  //Serial testing
  // for(int i = 0 ; i < MAX ; ++i){
  //   int prio = rand()%(MAX);
  //   pQueue.add(i,prio);
  //   cout<<"Inserted "<<i<<" with priority: "<<prio<<endl;
  // }

  // cout<<"\nRemoving the minimum number"<<endl;
  // for(int i  =0 ; i < MAX;  i++){
  //   cout<<pQueue.removeMin()<<endl;;
  // }

  //Parallel Testing

  const int TSIZE = 2;
  pthread_t threads[TSIZE];

  for(int i = 0 ; i < TSIZE ; ++i){
    if(i % 2 == 0){
      pthread_create(&threads[i],NULL, insert, NULL);
    }else{
      pthread_create(&threads[i],NULL,remove,NULL);
    }
  }

  for(int i = 0 ; i  < TSIZE; ++i){
    pthread_join(threads[i],NULL);
  }


  return 0;
}
