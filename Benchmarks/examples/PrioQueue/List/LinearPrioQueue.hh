#ifndef LIN_PRIO_QUEUE
#define LIN_PRIO_QUEUE

#include<iostream>
#include<cstdlib>
#include<vector>
using namespace std;

#include "QuasiQueue.hh"


class PrioQueue{
private:
  int range;
  vector<NQuasiQueue*> prio_buckets;
  Lock lock;

public:
  PrioQueue(int r);
  void add(int item, int prio);
  int removeMin();
};

PrioQueue::PrioQueue(int r ){
  range = r;
  prio_buckets.get_allocator().allocate(range);
  for(int i = 0; i < range; ++i){
    NQuasiQueue *node = new NQuasiQueue(4);
    prio_buckets.push_back(node);
  }
}


void PrioQueue::add(int item, int prio){
  prio_buckets[prio]->enqMethod(item);
}

int PrioQueue::removeMin(){
  int result = -999;
  for(int i = 0 ; i < range ; ++i){
    result = prio_buckets[i]->deqMethod();
    if(result != -999){
      return result;
    }
  }
  return result;
}

#endif
