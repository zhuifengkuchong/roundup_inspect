#ifndef PRIO_QUEUE_HH_
#define PRIO_QUEUE_HH_

#include<iostream>
#include<cstdlib>
#include<cmath>
#include<vector>

using namespace std;

#include "TreeNode.hh"
#include "Bin.hh"

class Prio{
private:
  int range;
  int height;
  TreeNode *head;
  vector<TreeNode*> leaves;

private:
  void calculateHeight(int input);
  TreeNode* createTree(int h, int slot);

public:
  Prio(int _range);
  void add(int item, int priority);
  int removeMinimum();

  //Debug Functions
  TreeNode* getHead();
  int getHeight();
  void doInorderTraversal(TreeNode*);
  void showAllElements();

};

Prio::Prio(int _range){
  range = _range;
  height = 0;
  head = NULL;
  leaves.get_allocator().allocate(range+1);
  calculateHeight(range);
  head = createTree(height,0);
}

void Prio::calculateHeight(int input){
  double numerator = log(input);
  double denominator = log(2);
  int result = numerator / denominator;
  height = 1+result;
}

TreeNode* Prio::createTree(int h, int slot){
  TreeNode *root= new TreeNode();
  if(h == 0){ 
    Bin *b = new Bin();
    root->setBin(b);
    if(slot == 0){
      leaves.insert(leaves.begin(),root);
    }else{
      leaves.insert((leaves.begin()+(slot-1)),root);
    }
  }else{
    root->setLeftChild(createTree((h-1),2*slot));
    root->setRightChild(createTree((h-1),(2*slot)+1));
    root->getLeftChild()->setParent(root);
    root->getRightChild()->setParent(root);
  }
  return root;
}

void Prio::add(int item, int priority){
  TreeNode *node = leaves[priority];
  node->getBin()->put(item);

  while(node != head){
    TreeNode *parent = node->getParent();
    if(node == parent->getLeftChild()){
      parent->getCounterRef()->getAndIncrement();
    }
    node = parent;
  }
}

int Prio::removeMinimum(){
  TreeNode *node = head;
  while(!node->isLeaf()){
    if(node->getCounterRef()->getAndDecrement() > 0 ){
      node = node->getLeftChild();
    }else{
      node = node->getRightChild();
    }
  }
  return node->getBin()->get();
}

TreeNode* Prio::getHead(){
  return head;
}

int Prio::getHeight(){
  return height;
}

void Prio::doInorderTraversal(TreeNode *node){
  if(node == NULL){
    return ;
  }

  doInorderTraversal(node->getLeftChild());
  cout<<node->getLeftChild()<<" : "<<node->getRightChild()<<endl;
  doInorderTraversal(node->getRightChild());
}

void Prio::showAllElements(){
  cout<<"\n Showing all leaves\n";
  vector<TreeNode*>::iterator itr = leaves.begin();
  int prio = 0;
  while(itr != leaves.end()){
    TreeNode *node = *itr;
    Bin *bin = node->getBin();
    cout<<"Priority : "<< prio <<"\t";
    bin->showBin();
    // *(itr)->getBin()->showBin();
    ++prio;
    ++itr;
  }
  cout<<endl;
}

#endif
