#ifndef TREE_NODE_HH_
#define TREE_NODE_HH_

#include<iostream>
#include<cstdlib>

using namespace std;

#include "../../AtomicInteger/AtomicInteger.hh"
#include "Bin.hh"

class TreeNode{

private:
  AtomicInteger counter;  
  TreeNode *left;
  TreeNode *right;
  TreeNode *parent;
  Bin *bin;

public:
  TreeNode();

  int getCounterValue();
  AtomicInteger* getCounterRef();

  void setParent(TreeNode *node);
  TreeNode* getParent();
  void setLeftChild(TreeNode *l);
  TreeNode* getLeftChild();
  void setRightChild(TreeNode *r);
  TreeNode* getRightChild();
  void setBin(Bin *b);
  Bin* getBin();

  bool isLeaf();
};

TreeNode::TreeNode(){
  left = NULL;
  right = NULL;
}


int TreeNode::getCounterValue(){
  return counter.get();
}

AtomicInteger* TreeNode::getCounterRef(){
  return &counter;
}

void TreeNode::setParent(TreeNode *node){
  parent = node;
}

TreeNode* TreeNode::getParent(){
  return parent;
}

void TreeNode::setLeftChild(TreeNode *l){
  left = l;
}

TreeNode* TreeNode::getLeftChild(){
  return left;
}

void TreeNode::setRightChild(TreeNode *r){
  right = r;
}

TreeNode* TreeNode::getRightChild(){
  return right;
}

void TreeNode::setBin(Bin *b){
  bin = b;
}

Bin* TreeNode::getBin(){
  return bin;
}

bool TreeNode::isLeaf(){
  if(right ==  NULL)
    return true;
  return false;
}

#endif
