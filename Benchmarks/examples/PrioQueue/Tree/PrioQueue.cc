#include<iostream>
#include<cstdlib>
#include<vector>

using namespace std;

#include<pthread.h>

#include "PrioQueue.hh"

const int MAX = 4;

Prio p(MAX);
vector<int> removedElements;
Lock lock;

void* doAddElements(void *data){
  int *i =(int *) data;
  int prio = rand()%MAX;
  p.add(*i,prio);
  // cout<<"Added : "<<*i<<" with Priority : "<<prio<<endl;
}

void* doRemoveElements(void *){
  lock.lock();
  removedElements.push_back(p.removeMinimum());
  lock.unlock();
}
void showRemovedElements(){
  vector<int>::iterator itr = removedElements.begin();
  while(itr != removedElements.end()){
    cout<<*itr<<"\t";
    itr++;
  }
  cout<<endl;
}
int main(){

  //Sequential Testing

  // cout<<"Height: "<<p.getHeight()<<endl;
  // // p.doInorderTraversal(p.getHead());
  
  // for(int i = 0 ; i< 10 ; i++){
  //   int prio = rand()%MAX;
  //   p.add(i,prio);
  //   cout<<"Added : "<<i<<" with Priority : "<<prio<<endl;
  // }

  // int item = -1;

  // for(int i = 0 ; i< 10 ; i++){
  //   cout<<p.removeMinimum()<<endl;
  // }


  //Parallel Testing

  const int MAX_THREADS = 10;
  pthread_t threads[MAX_THREADS];

  for(int i = 0 ;i <  MAX_THREADS; ++i){
    pthread_create(&threads[i],NULL,doAddElements,&i);
  }
  for(int i = 0 ;i <  MAX_THREADS; ++i){
    pthread_join(threads[i],NULL);
  }

  p.showAllElements();

  /* pthread_t removalThreads[MAX_THREADS];

 for(int k = 0 ;k <  MAX_THREADS; ++k){
    pthread_create(&removalThreads[k],NULL,doRemoveElements,NULL);
  }

  for(int j = 0 ;j <  MAX_THREADS; ++j){
    pthread_join(removalThreads[j],NULL);
    }*/

  //   showRemovedElements();

  return 0; 
}
