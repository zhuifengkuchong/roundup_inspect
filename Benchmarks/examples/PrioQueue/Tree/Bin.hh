#ifndef BIN_HH_
#define BIN_HH_

#include<iostream>
#include<deque>
using namespace std;

#include "Lock.hh"

class Bin{
private:
  deque<int> itemList;
  Lock lock;

public:
  Bin();
  void put(int x);
  int get();

  //Debug Functions
  void showBin();
};

Bin::Bin(){}

void Bin::put(int x){
  lock.lock();
  itemList.push_back(x);
  lock.unlock();
}

int Bin::get(){
  lock.lock();
  int  returnValue = itemList.front();
  itemList.pop_front();    
  lock.unlock();
  return returnValue;
}


void Bin::showBin(){
  deque<int>::iterator itr = itemList.begin();
  cout<<"Items : ";
  while(itr != itemList.end()){
    cout<<*itr<<"\t";
    itr++;
  }
  cout<<endl;
}

#endif
