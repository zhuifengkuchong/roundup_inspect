//-----------------------------------------------------------------------------
// Author:  Chao Wang <chaowang@vt.edu>
// Date:  12/27/2012
// 
// An example Linux/C++ implementation of the 'Integer' class used in
// the book: The Art of Multiprocessor Programming.
//-----------------------------------------------------------------------------

#ifndef  MP_INTEGER_HH
#define  MP_INTEGER_HH

class Integer {
  int value;
public:
  Integer(int val) { value = val; }
  ~Integer() { }

  void set(int val) { value = val; }
  int get() { return value; }
};

#endif // MP_INTEGER_HH
