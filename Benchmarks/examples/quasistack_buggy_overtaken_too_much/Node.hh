/*
 * Node.hh
 *
 *  Created on: Jan 3, 2013
 *      Author: arijit
 */

#ifndef NODE_HH_
#define NODE_HH_

#include<iostream>
#include<vector>
#include<ctime>
using namespace std;

#include "Lock.hh"

#define MAX_SIZE 2

#define UNINIT  0
#define VALID   1
#define REMOVED 2

//#define DBG_NODE true
#define DBG_NODE false
#define M false

//Check the error while compilation

class Node {
public:
  int elements[MAX_SIZE];
  int flag[MAX_SIZE];  // 0 - un-occupied   1 - occupied  2 - deleted
  int last_idx;        // monotonically increasing (never decreasing)
  int size;            // may increase or decrease
  Lock lock;

public:
  Node();
  void addElement(int elem);
  int getElement();
  void show();
  bool empty();
  bool full();
};


Node::Node() {
    for(int i=0; i<MAX_SIZE; i++) {
    flag[i] = UNINIT;
  }
  size = 0;
  last_idx = 0;
}

bool Node::full() {
  lock.lock();
  bool res = (last_idx == MAX_SIZE);
  lock.unlock();
  return res;
}

bool Node::empty() {
  lock.lock();
  bool res = (size == 0);        
  lock.unlock();
  return res;
}

void Node::show() {
  if (! DBG_NODE) return;
  cout << endl << "Node[" << ((long)this) << "] : {";
  for (int i = 0; i < MAX_SIZE; ++i) {
    if (flag[i] == UNINIT) 
      cout << "U" << "\t";
    else if (flag[i] == REMOVED)
      cout << "R" << "\t";
    else if (flag[i] == VALID) 
      cout << elements[i] << "\t";
    else 
      cout << "error" << "\t";
  }
  cout << " } ";
  cout << "size= " << size << " ";
  cout << "last_idx= " << last_idx << " ";
  cout << "MAX_SIZE= " << MAX_SIZE << endl;

}

void Node::addElement(int elem) {
  lock.lock();
  size ++;   // add one data item

  int position = last_idx;   // first un-occupied position
  last_idx++;
  //assert( position < MAX_SIZE );
  if (DBG_NODE && position >= MAX_SIZE) {
    cout << __FUNCTION__ << ": position >= MAX_SIZE" << endl;
  }

  elements[position] = elem;
  flag[position] = VALID;
  show();
  lock.unlock();
}


int Node::getElement() {
  lock.lock();
  int result = -999;
  int position = 0; 
  while ( position < MAX_SIZE && flag[position] != VALID ) {
    position ++;
  }
  if(M){
    cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Position found : "<<position<<endl;
  }

  if (position < MAX_SIZE) {
    result = elements[position];
    flag[position] = REMOVED;
    size --;  // remove one data item
    if(M){
      cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Position "<< position <<"Size  : "<<size<<endl;
    }
  }
  else {
    if (DBG_NODE) {
      cout << __FUNCTION__ << ": position <= -1 " << endl;
    }
  }
  
  show();
  lock.unlock();

  return result;
}


// int Node::getElement() {
//   lock.lock();

//   int result = -999;

//   int position = size - 1; 
//   while ( position >= 0 && flag[position] != VALID ) {
//     position -- ;
//   }

//   if (position > -1) {
//     result = elements[position];
//     flag[position] = REMOVED;
//     size --;  // remove one data item
//   }
//   else {
//     if (DBG_NODE) {
//       cout << __FUNCTION__ << ": position <= -1 " << endl;
//     }
//   }
  
//   show();
//   lock.unlock();

//    return result;
// }


#endif /* NODE_HH_ */
