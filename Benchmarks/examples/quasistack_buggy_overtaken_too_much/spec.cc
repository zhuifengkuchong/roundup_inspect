#include<iostream>
#include<vector>
#include<cstdlib>
using namespace std;
#include<pthread.h>
#include "Lock.hh"

class QuasiStack{
private:
  int capacity;
  int top;
  Lock lock;
  vector<int> nodes;

public:
  QuasiStack(int cap);
  void push(int item);
  int pop();
};

QuasiStack::QuasiStack(int cap){
  capacity = cap;
  top = 0;
  nodes.get_allocator().allocate(capacity);
}

void QuasiStack::push(int item){
  lock.lock();
  if(top == capacity){
    cout<<"Stack is full"<<endl;
    lock.unlock();   
  }else{
    nodes.push_back(item);
    ++top;
    lock.unlock();
  }
}

int QuasiStack::pop(){
  int result = -999;
  lock.lock();
  if(top == 0){
    cout<<"Stack is empty"<<endl;
    lock.unlock();
  }else{
    result = nodes[top-1];
    nodes.pop_back();
    --top;
    lock.unlock();
  }
  return result;
}


QuasiStack qStack(6);

void* thread1(void*){
  qStack.push(1);
  cout<<"Pushed : "<<1<<endl;  
  qStack.push(2);
  cout<<"Pushed : "<<2<<endl;
}

void* thread2(void*){
  qStack.push(3);
  cout<<"Pushed : "<<3<<endl;

  int item = qStack.pop();
  cout<<"               Popped : "<<item<<endl;
}


int main(){
  pthread_t t1, t2;
  qStack.push(0);
  cout<<"Pushed : "<<0<<endl;

  pthread_create(&t2,NULL,thread2,NULL);
  pthread_create(&t1,NULL,thread1,NULL);

  pthread_join(t1,NULL);
  pthread_join(t2,NULL);

  int item;

  item = qStack.pop();
  cout<<"               Popped : "<<item<<endl;
  item = qStack.pop();
  cout<<"               Popped : "<<item<<endl;
  item = qStack.pop();
  cout<<"               Popped : "<<item<<endl;
  return 0;
}
