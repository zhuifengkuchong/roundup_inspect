/*
 * Node.hh
 *
 *  Created on: Jan 3, 2013
 *      Author: arijit
 */

#ifndef NODE_HH_
#define NODE_HH_

#include<iostream>
#include<vector>
#include<ctime>
using namespace std;

#include "Lock.hh"

//Check the error while compilation

class Node {
public:
  vector<int> elements;
  Lock lock;
  static const int MAX_SIZE;

public:
  Node();
  void addElement(int elem);
  int getElement();
  void show();
  int getBucketSize();
  int getMaxBucketSize();
};

//Default Constructor
const int Node::MAX_SIZE = 2;


Node::Node() {
  srand(time(0)); 
  elements.get_allocator().allocate(MAX_SIZE);
}

void Node::show() {
  for (int i = 0; i < elements.size(); ++i) {
    cout << elements[i] << "\t";
  }
}


void Node::addElement(int elem) {
  lock.lock();
  elements.push_back(elem);
  lock.unlock();
}


int Node::getElement() {
  int result = -999;
  lock.lock();
  int position ;
  int size = elements.size();
  if(size == 0 ){
    lock.unlock();
    return result;
  }
	  
  //position = rand()%elements.size();
  position = elements.size()-1;                 //chao: remove non-determinism
 
  result = elements[position];
  if(position == 0){
    elements.erase(elements.begin());
  }else{
    elements.erase(elements.begin()+(position-1));   //chao: this is a bug
  }
  // cout<<"Successful DEletion"<<endl;
  lock.unlock();
  return result;
}

int Node::getBucketSize(){
  return elements.size();
}

int Node::getMaxBucketSize(){
  return MAX_SIZE;
}
#endif /* NODE_HH_ */
