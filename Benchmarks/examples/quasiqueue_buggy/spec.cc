#include<iostream>
#include<vector>
#include<cmath>
#include<cstdlib>
using namespace std;
#include<pthread.h>
#include "Lock.hh"

class NQuasiQueue {
private:
  int capacity;
  int head;
  int tail;
  Lock lock;
  vector<int> nodes;

public:
  NQuasiQueue(int _cap);
  void enqMethod(int item);
  int deqMethod();
  void showEntireQueue();
};

NQuasiQueue::NQuasiQueue(int _cap) {
  head = tail = 0;
  capacity = _cap;
  nodes.get_allocator().allocate(capacity);
}


void NQuasiQueue::showEntireQueue() {
  for (int i = head; i < tail; ++i) {
    cout<<nodes[i]<<"\t";
  }
  cout<<endl;
}


void NQuasiQueue::enqMethod(int item) {
  lock.lock();
  if (capacity == abs(tail-head)) {
    cout<<"Can not enque in a full queue"<<endl;
    lock.unlock();
    return;
  }
  int position = tail % capacity;
  nodes.push_back(item);
  ++tail;
  lock.unlock();
}

int NQuasiQueue::deqMethod() {
  lock.lock();
  if (head == tail) {
      cout<<"Can not deque from empty queue"<<endl;
      lock.unlock();
      return -999;
  }

  int tempNode =  nodes[head%capacity];
   ++head;
  lock.unlock();
  return tempNode;
}





NQuasiQueue pQueue(6);



void* thread1(void *)
{
  {
    pQueue.enqMethod(1);  
    cout<<"Enqueued : "<< 1 <<endl;
  }
  {
    pQueue.enqMethod(2);  
    cout<<"Enqueued : "<< 2 <<endl;
  }
  return 0;
}


void* thread2(void *)
{
  {
    pQueue.enqMethod(3);  
    cout<<"Enqueued : "<< 3 <<endl;
  }
  {
    int item = pQueue.deqMethod();  
    cout<<"                 Dequed : "<<item<<endl;
  }
  return 0;
}


int main(){

  pthread_t t1, t2;

  pQueue.enqMethod(0);  
  cout<<"Enqueued : "<< 0 <<endl<<endl;

  pthread_create( &t2, 0, thread2, 0);
  pthread_create( &t1, 0, thread1, 0);

  pthread_join( t1, 0 );
  pthread_join( t2, 0 );

  cout<<endl; 

  int item;
  item = pQueue.deqMethod();  
  cout<< "                 Dequed : "<<item<<endl;
  item = pQueue.deqMethod();  
  cout<<"                 Dequed : "<<item<<endl;
  item = pQueue.deqMethod();  
  cout<<"                 Dequed : "<<item<<endl;

  return 0;
}
