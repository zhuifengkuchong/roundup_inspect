// Copyright (c) 2012-2013, the Scal Project Authors.  All rights reserved.
// Please see the AUTHORS file for details.  Use of this source code is governed
// by a BSD license that can be found in the LICENSE file.

#define __STDC_FORMAT_MACROS 1  // we want PRIu64 and friends
#include <gflags/gflags.h>
#include <pthread.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include "benchmark/common.h"
#include "benchmark/std_glue/std_pipe_api.h"
#include "datastructures/pool.h"
#include "util/malloc.h"
#include "util/operation_logger.h"
#include "util/random.h"
#include "util/threadlocals.h"
#include "util/time.h"
#include "util/workloads.h"
#include "datastructures/flatcombining_queue.h"

#include <iostream>
using namespace std;

DEFINE_string(prealloc_size, "4mb", "tread local space that is initialized");
DEFINE_uint64(producers, 1, "number of producers");
DEFINE_uint64(consumers, 1, "number of consumers");
DEFINE_uint64(operations, 2, "number of operations per producer");
DEFINE_uint64(c, 10, "computational workload");
DEFINE_bool(print_summary, true, "print execution summary");
DEFINE_bool(log_operations, false, "log invocation/response/linearization "
		"of all operations");

DEFINE_uint64(kk, 0, "quasi factor");

using scal::Benchmark;

class ProdConBench: public Benchmark {
public:
	ProdConBench(uint64_t num_threads, uint64_t thread_prealloc_size,
			uint64_t histogram_size, void *data) :
			Benchmark(num_threads, thread_prealloc_size, histogram_size, data) {
	}
protected:
	void bench_func(void);
	void bench_func1(void);

private:
	void producer(void);
	void consumer(void);
};

//pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

uint64_t g_num_threads;

//MSQueue<uint64_t> *ds;
//SingleList<uint64_t> *ds = new SingleList<uint64_t>();
//RandomDequeueQueue<uint64_t> *ds=new RandomDequeueQueue<uint64_t>(FLAGS_kk+1, 10);
//TreiberStack<uint64_t> *ds;
//KStack<uint64_t> *ds;
//UnboundedSizeKFifo<uint64_t> *ds;
FlatCombiningQueue<uint64_t> *ds;

void zl_pre();
void zl_post();
vector<string> split_string(string input, string split_by);
int num_put_pre = 3;
int num_get_pre = 0;

int num_put_post = 0;
int num_get_post;

int num_put_T1 = 1;
int num_get_T1 = 1;

int num_put_T2 = 1;
int num_get_T2 = 1;

void init_ds() {
//	ds = new MSQueue<uint64_t>();
//	ds = new TreiberStack<uint64_t>();
//	ds = new KStack<uint64_t>(FLAGS_kk+1, g_num_threads + 1);
	ds = new FlatCombiningQueue<uint64_t>(6);

	char* filter_file = "../run_config.txt";
	fstream fin(filter_file);
	string line;

	vector<string> config;
	while (getline(fin, line)) {

		config = split_string(line, ":");

		if (strstr(config[1].c_str(), "num_put_pre")) {
			num_put_pre = std::stoi(config[0]);
		} else if (strstr(config[1].c_str(), "num_get_pre")) {
			num_get_pre = std::stoi(config[0]);
		} else if (strstr(config[1].c_str(), "num_put_post")) {
			num_put_post = std::stoi(config[0]);
		} else if (strstr(config[1].c_str(), "num_put_T1")) {
			num_put_T1 = std::stoi(config[0]);
		} else if (strstr(config[1].c_str(), "num_get_T1")) {
			num_get_T1 = std::stoi(config[0]);
		} else if (strstr(config[1].c_str(), "num_put_T2")) {
			num_put_T2 = std::stoi(config[0]);
		} else if (strstr(config[1].c_str(), "num_get_T2")) {
			num_get_T2 = std::stoi(config[0]);
		} else {
			continue;
		}
	}
	fin.close();
	num_get_post = num_put_pre + num_put_post + num_put_T1 + num_put_T2
			- num_get_pre - num_get_T1 - num_get_T2;
}

int main(int argc, const char **argv) {

	google::ParseCommandLineFlags(&argc, const_cast<char***>(&argv), true);

	uint64_t tlsize = scal::human_size_to_pages(FLAGS_prealloc_size.c_str(),
	                                              FLAGS_prealloc_size.size());

	g_num_threads = FLAGS_producers + FLAGS_consumers;
	scal::tlalloc_init(tlsize, true /* touch pages */);

	scal::ThreadContext::prepare(g_num_threads + 1);
	scal::ThreadContext::assign_context();

	cout << "Quasi Factor =  " << FLAGS_kk << endl;

	zl_pre();

	ProdConBench *benchmark = new ProdConBench(g_num_threads, tlsize,
			FLAGS_operations * (g_num_threads + 1), ds);
	benchmark->run();

	zl_post();

	return EXIT_SUCCESS;
}

void zl_pre() {

	init_ds();

	for (int i = 1; i <= num_put_pre; i++) {
		ds->zl_put(i);
	}
	for (int i = 1; i <= num_get_pre; i++) {
		ds->zl_get();
	}

}

void zl_post() {

	for (int i = 1; i <= num_put_post; i++) {
		ds->zl_put(i + 10000);
	}
	for (int i = 1; i <= num_get_post; i++) {
		ds->zl_get();
	}

}

void ProdConBench::producer(void) {

	for (int i = 1; i <= num_put_T1; i++) {
		ds->zl_put(i + 100);
	}
	for (int i = 1; i <= num_get_T1; i++) {
		ds->zl_get();
	}
}

void ProdConBench::consumer(void) {

	for (int i = 1; i <= num_put_T2; i++) {
		ds->zl_put(i + 200);
	}
	for (int i = 1; i <= num_get_T2; i++) {
		ds->zl_get();
	}
}

void ProdConBench::bench_func(void) {

	producer();

}

void ProdConBench::bench_func1(void) {

	consumer();

}

vector<string> split_string(string input, string split_by) {

	vector<string> str_list;
	if (input.size() < 1)
		return str_list;
	int comma_n = 0;
	do {
		std::string tmp_s = "";
		comma_n = input.find(split_by);
		if (-1 == comma_n) {
			tmp_s = input.substr(0, input.length());
			str_list.push_back(tmp_s);
			break;
		}
		tmp_s = input.substr(0, comma_n);
		input.erase(0, comma_n + split_by.size());
		str_list.push_back(tmp_s);
	} while (true);
	return str_list;
}

