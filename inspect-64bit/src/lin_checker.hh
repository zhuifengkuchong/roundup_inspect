/*
 * linchecker.hh
 *
 *  Created on: Dec 8, 2012
 *      Author: jack
 */
#include <iostream>
#include <map>
#include "inspect_event.hh"
#include "event_buffer.hh"
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>

#ifndef LINCHECKER_HH_
#define LINCHECKER_HH_

using namespace std;

class quasi_state {
public:
//	quasi_state(){
//		selected_eid=0;
//		must_select=0;
//	}
//
//

	map<int,bool> enable_done;
	map<int,int> quasi_cost;
	map<int,bool> new_enable_done;
	int selected_eid;
	int must_select;

	string to_string() {
		stringstream ss;
		ss << "Enable_done: { ";
		for (map<int, bool>::iterator it = enable_done.begin();
				it != enable_done.end(); it++) {

			if (it->second) {
				ss << it->first << ", ";
			} else {
				ss << it->first << ":done, ";
			}

		}
		ss << "} ";

		ss << "  Quasi_cost: { ";
		for (map<int, int>::iterator it = quasi_cost.begin();
				it != quasi_cost.end(); it++) {

			ss << it->first << ":" << it->second << ", ";
		}
		ss << "} ";

		ss<<"  Select event: "<<selected_eid<<"  Must select: "<<must_select;

		return ss.str();
	}

};

class lin_checker {
public:
	lin_checker();

	virtual ~lin_checker();

	void add_to_trace(InspectEvent &event);

	void remove_serial_duplication();

	bool duplicate_concur_trace(char* fname, char* dir);

	void trace_dump_to_file(map<int, InspectEvent> &curr_trace);

	void trace_dump_to_file(map<int, InspectEvent> &curr_trace, char* path);

	bool step6_quasi_trace_subTrace(map<int, InspectEvent> &curr_trace,string test_function);

	vector<map<int, int> > step7_get_One_sub_quasi_traces(map<int, InspectEvent> &curr_trace,string function);

	map<int, InspectEvent> gen_trace_from_file(char* path_file);

	set<string> read_filters();

	vector<string> split_string(string input, string split_by);

	bool step4_check_learization(map<int, InspectEvent> &curr_trace);

	void print_trace(map<int, InspectEvent> &curr_trace);

	void step5_get_backTrack_info(map<int, InspectEvent> &curr_trace);

	void split_check_trace();

	bool check_trace_linearizability(map<int, InspectEvent> &curr_trace);

	void step1_lin_check_entrance();

	void step2_quasi_split_check_trace();

	bool linearizable_check_by_diff();

	void print_trace2(map<int, InspectEvent> &trace);

	void clear();

	vector<map<int, InspectEvent> > step3_compute_linearization(map<int, InspectEvent> &trace);

	map<int, quasi_state> new_trace_from_state(quasi_state &from_state, int &index,
			map<int, set<int> > &new_enabled);
	bool quasi_check_two_traces_same(map<int, InspectEvent>& trace1, map<int, InspectEvent>& trace2);

	static string file_to_string(char* file);
	static bool compare_file(char* file1, char* file2);
	bool quasi_check_with_serial_traces(map<int, InspectEvent>& quazi_trace);

	string trim(string s);

	string remove_head_and_tail(string s);

	set<string> function_filter;

public:

	map<int, InspectEvent> concurr_trace;
//	map<int, InspectEvent> concurr_trace2;

	map<string, int> function_count;

	vector<map<int, InspectEvent> > traces;

	string log_file_name;



};

/* namespace std */
#endif /* LINCHECKER_HH_ */
